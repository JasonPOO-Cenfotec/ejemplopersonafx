package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.entities.Persona;
import cr.ac.ucenfotec.bl.logic.PersonaGestor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdministradorController implements Initializable {

    private Persona usuarioSesion;
    private String mensaje;
    private PersonaGestor gestor = new PersonaGestor();
    ObservableList<Object> datos = FXCollections.observableArrayList();

    @FXML
    private TableView tblPersonas;

    @FXML
    private TableColumn colUno;

    @FXML
    private TableColumn colDos;

    @FXML
    private TableColumn colTres;

    @FXML
    private TableColumn colCuatro;

    @FXML
    private Label lblTitulo;

    @FXML
    private TextField txtCedula;

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtCorreo;

    @FXML
    private TextField txtPassword;


    public Persona getUsuarioSesion() {
        return usuarioSesion;
    }

    public void setUsuarioSesion(Persona usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
        setTitulo();
    }

    private void setTitulo(){
        lblTitulo.setText("Bienvenido " + usuarioSesion.getCedula() + " - " + usuarioSesion.getNombre());
    }

    @FXML
   public void salir(ActionEvent event) throws IOException{
        Parent parentScene = FXMLLoader.load(getClass().getResource("../ui/Login.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            cargarListaUsuarios();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    private void cargarListaUsuarios() throws Exception{

        tblPersonas.getItems().clear();
        gestor.listarPersonas().forEach(persona -> datos.addAll(persona));

        colUno.setCellValueFactory(new PropertyValueFactory<>("cedula"));
        colDos.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colTres.setCellValueFactory(new PropertyValueFactory<>("correo"));
        colCuatro.setCellValueFactory(new PropertyValueFactory<>("password"));

        tblPersonas.setItems(datos);
    }

    public void cargarUsuario(){
        Persona usuario = (Persona)tblPersonas.getSelectionModel().getSelectedItem();
        if(usuario !=null){
            txtCedula.setText(""+usuario.getCedula()+"");
            txtNombre.setText(usuario.getNombre());
            txtCorreo.setText(usuario.getCorreo());
            txtPassword.setText(usuario.getPassword());
        }
    }

    public void registrarUsuario() throws Exception {
        mensaje = gestor.registrarPersona(Integer.parseInt(txtCedula.getText()),txtNombre.getText(),txtCorreo.getText(),txtPassword.getText());
        cargarListaUsuarios();
        mostrarMensaje(mensaje);
    }

    public void modificarUsuario() throws Exception {
        mensaje = gestor.modificarPersona(Integer.parseInt(txtCedula.getText()),txtNombre.getText(),txtCorreo.getText(),txtPassword.getText());
        cargarListaUsuarios();
        mostrarMensaje(mensaje);
    }

    public void eliminarUsuario() throws Exception {
        mensaje = gestor.eliminarPersona(Integer.parseInt(txtCedula.getText()));
        cargarListaUsuarios();
        mostrarMensaje(mensaje);
    }

    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }
}
