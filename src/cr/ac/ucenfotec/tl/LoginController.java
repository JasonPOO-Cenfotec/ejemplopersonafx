package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.entities.Persona;
import cr.ac.ucenfotec.bl.logic.PersonaGestor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private PersonaGestor gestor = new PersonaGestor();

    @FXML
    private TextField txtCorreo;

    @FXML
    private PasswordField txtPassword;

    @FXML
    void validarDatos(ActionEvent event) {
        String correo = txtCorreo.getText();
        String contrasena = txtPassword.getText();
        Persona usuario;
        boolean error = false;

        error = validarCampos(correo, contrasena);

        if (error) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Por favor complete los campos");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else {
            usuario = buscarUsuario(correo, contrasena);
            if (usuario !=null) {
                cambiarEscena(usuario,event);
            }
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR, "El usuario no se encuentra registrado");
                alert.setHeaderText(null);
                alert.showAndWait();
            }
        }
    }

    private boolean validarCampos(String correo, String contrasena) {
        boolean error = false;

        if (correo.equals("")) {
            error = true;
        }
        if (contrasena.equals("")) {
            error = true;
        }
        return error;
    }

    private Persona buscarUsuario(String correo, String password) {
        try {
            Persona usuario = gestor.buscarPersona(correo,password);
            if(usuario != null) {
                    return usuario;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void cambiarEscena(Persona usuario, ActionEvent event){
        try {
            switch (usuario.getTipo()) {
                case "Administrador":

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/AdminInicio.fxml"));
                    Parent parentScene = loader.load();
                    AdministradorController controller = loader.getController();
                    controller.setUsuarioSesion(usuario);

                    Scene newScene = new Scene(parentScene);
                    Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
                    window.setScene(newScene);
                    window.show();
                    break;

                case "Cliente":
                    break;
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Error al cambiar de ventana. Intente de nuevo: " + e.getMessage());
            alert.setHeaderText(null);
            alert.showAndWait();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
