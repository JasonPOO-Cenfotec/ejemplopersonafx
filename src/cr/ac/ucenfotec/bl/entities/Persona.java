package cr.ac.ucenfotec.bl.entities;

public class Persona {

    private int cedula;
    private String nombre;
    private String correo;
    private String password;
    private String tipo;

    public Persona() {
    }

    public Persona(int cedula, String nombre, String correo, String password) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.correo = correo;
        this.password = password;
        this.tipo = "Administrador";
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if(!(o instanceof Persona)) return false;
        Persona persona = (Persona) o;
        return cedula == persona.cedula;
    }

    public String toString() {
        return "cedula="+ cedula +", nombre="+ nombre+ ", correo="+ correo +", password="+ password;
    }
}
