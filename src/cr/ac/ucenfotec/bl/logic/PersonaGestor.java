package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.Persona;
import cr.ac.ucenfotec.dl.Data;

import java.util.ArrayList;

public class PersonaGestor {

    private Data datos;

    public PersonaGestor(){
        datos = new Data();
    }

    public ArrayList<Persona> listarPersonas() throws Exception{
        return  datos.listarPersonas();
    }

    public Persona buscarPersona(String correo, String password) throws Exception{
        Persona persona = new Persona();
        persona.setCorreo(correo);
        persona.setPassword(password);
        return datos.buscarPersona(persona);
    }

    public String registrarPersona(int cedula, String nombre, String correo, String password) throws Exception{
        Persona persona = new Persona(cedula,nombre,correo,password);
       return datos.agregarPersona(persona);
    }

    public String modificarPersona(int cedula, String nombre, String correo, String password) throws Exception{
        Persona persona = new Persona(cedula,nombre,correo,password);
        return datos.modificarPersona(persona);
    }

    public String eliminarPersona(int cedula) throws Exception{
        Persona persona = new Persona();
        persona.setCedula(cedula);
        return datos.eliminarPersona(persona);
    }
}
