package cr.ac.ucenfotec.dl;

import cr.ac.ucenfotec.bl.entities.Persona;
import java.util.ArrayList;

public class Data {

    private static ArrayList<Persona> lista;

    public Data(){
        lista = new ArrayList<>();
        cargarPersonas();
    }

    public static ArrayList<Persona> getLista(){
        cargarPersonas();
        return lista;
    }

    private static void cargarPersonas(){
        lista.add(new Persona(123,"Jason","abc@gmail.com","123"));
        lista.add(new Persona(456,"María","def@gmail.com","456"));
        lista.add(new Persona(789,"Pedro","ghi@gmail.com","798"));
        lista.add(new Persona(147,"Ana","jkl@gmail.com","147"));
    }

    public ArrayList<Persona> listarPersonas() throws Exception {
        return (ArrayList<Persona>) lista.clone();
    }

    public Persona buscarPersona(Persona persona) throws Exception {
        for (Persona personaTemp : lista) {
            if( personaTemp.getCorreo().equals(persona.getCorreo()) &&
                    personaTemp.getPassword().equals(persona.getPassword())){
                return personaTemp;
            }
        }
        return null;
    }

    public String agregarPersona(Persona persona) throws Exception {
        lista.add(persona);
        return "Persona agregada de manera correcta!";
    }

    public String modificarPersona(Persona persona) throws Exception {
        int posicion = 0;
        for (Persona personaTemp : lista) {
            if(personaTemp.equals(persona)){
                lista.set(posicion,persona);
                return "Persona modificada de manera correcta!";
            }
            posicion++;
        }
        return "La persona no existe, no es posible modificar sus datos!";
    }

    public String eliminarPersona(Persona persona) throws Exception {
        lista.remove(persona);
        return "Persona eliminada de manera correcta!";
    }

}
